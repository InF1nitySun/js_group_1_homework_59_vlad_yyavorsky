import React, {Component} from 'react';
import './App.css';
import AddForm from "./components/AddForm/AddForm";
import MainList from "./components/MainField/MainList";

let itemId = 5;

class App extends Component {

    state = {
        items: [
            {name: 'Черная Пантера', id: 1},
            {name: 'Мстители: Война бесконечности', id: 2},
            {name: 'Дэдпул 2', id: 3},
            {name: 'Человек-Муравей и Оса', id: 4}
        ],
        name: ''
    };


    changeTaskName = (e) => {
        let name = e.target.value;
        this.setState({name});
    };

    changeName = (event, id) => {
        const items = [...this.state.items];
        let index = items.findIndex(i => i.id === id);
        const item = {...this.state.items[index]};
        item.name = event.target.value;
        items[index] = item;
        this.setState({items: items});
    };


    addTask = () => {
        let items = [...this.state.items];
        let name = this.state.name;

        if (name) {
            items.push({name, id: itemId});

            itemId++;
            this.setState({items});
        } else {
            alert('проверьте введенные данные');
        }
    };

    remTask = (id) => {
        let items = [...this.state.items];
        const index = items.findIndex(item => item.id === id);
        items.splice(index, 1);
        this.setState({items});
    };

    render() {
        return (
            <div className="App">
                <h3>HomeWork 59</h3>
                <h5>Part 1</h5>
                <div className='MainFiled'>
                    <AddForm change={this.changeTaskName} click={this.addTask}/>
                    <p>To Whatch List:</p>
                    <MainList items={this.state.items} click={this.remTask} change={this.changeName}/>
                </div>
            </div>
        );
    }
}

export default App;
