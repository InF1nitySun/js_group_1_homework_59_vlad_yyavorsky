import React from 'react';
import './AddForm.css'


const AddForm = props => {
    return (
        <div className='AddForm'>
            <input onChange={props.change} type="text" placeholder='Please input the Film Name'/>
            <button className='AddButton' onClick={props.click}>Add</button>
        </div>
    )
};

export default AddForm;