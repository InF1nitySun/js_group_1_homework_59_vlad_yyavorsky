import React from 'react';
import './Task.css';

const Task = props => {
    return (
        <div className='Item'>
            <p ><input className='Name' type="text" value={props.name} onChange={props.change}/></p>
            <button className='RemButton' onClick={props.click}>X</button>
        </div>
    )
};

export default Task;