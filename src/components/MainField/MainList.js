import React from 'react';
import Task from "./Task/Task";
import './MainList.css'


const MainList= props => {
    return (
        <div className='MainList'>
                {props.items.map(item => {
                    return <Task name={item.name} click={() => props.click(item.id)} key={item.id}
                                 change={(event) => props.change(event, item.id)}
                    />
                } )}

        </div>
    )
};

export default MainList;